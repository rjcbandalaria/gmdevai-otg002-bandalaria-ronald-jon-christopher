﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float movementSpeed;

    public CharacterController playerController;

    public Vector3 velocity;
    public float gravity = -9.81f;
    public Transform feet;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    bool isGrounded;

    public float jumpHeight = 3f;

    // Update is called once per frame
    void Update()
    {

        isGrounded = Physics.CheckSphere(feet.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {

            velocity.y = -2f;

        }

       

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        //move = move.normalized;

        playerController.Move(move * movementSpeed * Time.deltaTime);

        if(Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);

        }

        velocity.y += gravity * Time.deltaTime;

        playerController.Move(velocity * Time.deltaTime);

    }

    
}
