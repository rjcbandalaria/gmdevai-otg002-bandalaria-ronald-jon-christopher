﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{

    GameObject[] agents;

    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0)) // Gets mouse input 
        //{

        //    RaycastHit hit; 

        //    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000)) // when the left mouse button is pressed 
        //    {
        //        foreach (GameObject ai in agents)
        //        {
        //            ai.GetComponent<AIControl>().agent.SetDestination(hit.point); // sets the bots destination to the mouse click 
        //        }
        //    }

        //}

        foreach(GameObject ai in agents)
        {

            if(Vector3.Distance(ai.transform.position, player.transform.position) > 5)
            {

                ai.GetComponent<AIControl>().agent.SetDestination(player.transform.position);

            }
            

        }

    }
}
