﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconSpawn : MonoBehaviour
{
    public GameObject beacon;

    GameObject[] agents;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        // if monster spawn
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                Instantiate(beacon, hit.point, beacon.transform.rotation);
                foreach (GameObject a in agents)
                {
                    // agents should be running away from the monster 
                    a.GetComponent<AIControl>().flockBeacon(hit.point);
                }
            }
        }
    }
}
