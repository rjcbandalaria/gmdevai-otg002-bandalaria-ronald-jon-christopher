﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  
    // Update is called once per frame
    void Update()
    {
        QuitGame();
    }


    void QuitGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Quit Program");
            Application.Quit();

        }
    }
}
