﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideZombie : MonoBehaviour
{
    AIBehavior aiController;

    // Start is called before the first frame update
    void Start()
    {
        aiController = GetComponent<AIBehavior>();
    }

    // Update is called once per frame
    void Update()
    {
        if (aiController.canSeeTarget())
        {
            //Debug.Log("Hide");
            aiController.CleverHide();
        }
        else
        {
            aiController.Wander();
        }
    }
}
