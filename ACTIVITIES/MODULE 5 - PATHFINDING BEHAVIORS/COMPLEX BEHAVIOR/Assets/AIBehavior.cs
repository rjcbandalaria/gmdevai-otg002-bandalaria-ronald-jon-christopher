﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehavior : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement playerMovement;

    Vector3 wanderTarget;

    public Vector3 sightRange;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
    }

    void Seek(Vector3 location)
    {

        agent.SetDestination(location);

    }

    void Flee(Vector3 location)
    {
        // inverse to the seek function to run away from the player 
        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);


    }

    public void Pursue()
    {
        // Getting the direction of the target
        Vector3 targetDirection = target.transform.position - this.transform.position;

        //to predict the player's next position and modifying the speed the enemy travels
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Seek(target.transform.position + target.transform.forward * lookAhead);

    }

    public void Evade()
    {

        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * lookAhead);

    }

    public void Wander()
    {

        float wanderRadius = 20f;
        float wanderDistance = 10f;
        float wanderJitter = 1f; // how often or how big the AI would deviate from its current direction to wander to another direction

        //generate a random jitter wherever the player goes  
        //Only happens on the x and z axis
        //Use float 
        wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter,
                                    0,
                                    Random.Range(-1.0f, 1.0f) * wanderJitter);
        //Normalize
        wanderTarget.Normalize();

        //Bigger or smaller area to wander 
        wanderTarget *= wanderRadius;

        // set local target where the Seek function can be used to go to the location
        // setting first to local for the agent
        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);

        //transform local vector to global vector 
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);

    }

    public void Hide()
    {

        float distance = Mathf.Infinity; // temp distance    
        Vector3 chosenSpot = Vector3.zero; // no hiding spot yet 

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        // find the nearest hiding spot
        for (int i = 0; i < hidingSpotsCount; i++)
        {

            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;

            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; // distance offset 

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                distance = spotDistance;
            }

        }

        Seek(chosenSpot);

    }

    public void CleverHide()
    {

        float distance = Mathf.Infinity; // temp distance    
        Vector3 chosenSpot = Vector3.zero; // no hiding spot yet 
        Vector3 chosenDirection = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0];

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        // find the nearest hiding spot
        for (int i = 0; i < hidingSpotsCount; i++)
        {

            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;

            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; // distance offset 

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                chosenDirection = hideDirection;
                chosenGameObject = World.Instance.GetHidingSpots()[i];
                distance = spotDistance;
            }

        }

        Collider hideCollider = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDirection.normalized);
        RaycastHit info;
        float rayDistance = 100f;
        hideCollider.Raycast(back, out info, rayDistance);

        Seek(info.point + chosenDirection.normalized * 5);

    }

    public bool canSeeTarget()
    {
        // shoots a raycast from target 
        RaycastHit raycastInfo;
        Vector3 rayToTarget = //sightRange - this.transform.position;
            target.transform.position - this.transform.position;
        //Debug.DrawLine(this.transform.position, rayToTarget, Color.red);
        if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo))
        {
            // if player gets hit by raycast then it returns true 
        
            if(raycastInfo.transform.gameObject.tag == "Player")
            {
                Debug.Log(raycastInfo.transform.gameObject.tag + " Detected");
                
                return true;
                //return raycastInfo.transform.gameObject.tag == "Player";
            }

        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {

        //Flee(target.transform.position);
        //Pursue();
        //Evade();
        //Wander();
        //Hide();
        //CleverHide();
        /*if (canSeeTarget())
        {

           CleverHide();

        }*/

        /*if (canSeeTarget())
        {
            Pursue();
        }
        else
        {
            Wander();
        }*/

    }
}
