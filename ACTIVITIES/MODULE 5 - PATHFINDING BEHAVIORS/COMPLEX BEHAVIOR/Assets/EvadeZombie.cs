﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeZombie : MonoBehaviour
{
    // Start is called before the first frame update
    AIBehavior aiController;
    void Start()
    {
        aiController = GetComponent<AIBehavior>();
    }

    // Update is called once per frame
    void Update()
    {

        if (aiController.canSeeTarget())
        {
            aiController.Evade();
        }
        else
        {
            aiController.Wander();
        }

    }
}
