﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Transform goal;

    public float vehicleSpeed = 0;

    public float rotationSpeed = 2;

    public float acceleration = 5;

    public float deceleration = 5;

    public float minSpeed = 0;

    public float maxSpeed = 10;

    public float brakeAngle = 20;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        //Vector3 lookAtGoal = new Vector3(goal.position.x,
        //    this.transform.position.y, goal.position.z);

        //Vector3 direction = lookAtGoal - this.transform.position;

        //this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
        //    Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

        VehicleTurn();

        if (Vector3.Angle(goal.forward, this.transform.forward) > brakeAngle && vehicleSpeed > 0.5) // brakes if the angle of the car and the goal is greater than the braking angle 
        {
            //vehicleSpeed = Mathf.Clamp(vehicleSpeed - (deceleration * Time.deltaTime),
            //    minSpeed, maxSpeed);
            DecelerateVehicle();

        }
        else
        {
            //vehicleSpeed = Mathf.Clamp(vehicleSpeed + (acceleration * Time.deltaTime)
            //    , minSpeed, maxSpeed);
            AccelerateVehicle();
        }

        //Debug.Log(vehicleSpeed);


        //this.transform.Translate(0, 0, vehicleSpeed);
        MoveVehicle();
    }

    void VehicleTurn()
    {

        Vector3 lookAtGoal = new Vector3(goal.position.x,
           goal.position.y, goal.position.z);

        Vector3 direction = lookAtGoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
            Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

    }

    void DecelerateVehicle()
    {
        vehicleSpeed = Mathf.Clamp(vehicleSpeed - (deceleration * Time.deltaTime),
                minSpeed, maxSpeed);
    }

    void AccelerateVehicle()
    {

        vehicleSpeed = Mathf.Clamp(vehicleSpeed + (acceleration * Time.deltaTime)
               , minSpeed, maxSpeed);

    }

    void MoveVehicle()
    {

        this.transform.Translate(0, 0, vehicleSpeed);

    }


 }
