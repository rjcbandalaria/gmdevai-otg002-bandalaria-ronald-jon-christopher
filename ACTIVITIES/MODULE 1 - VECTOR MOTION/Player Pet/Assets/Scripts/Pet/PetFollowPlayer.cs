﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Sockets;
using UnityEngine;

public class PetFollowPlayer : MonoBehaviour
{

    public Transform player;
    public float movementSpeed;
    public float rotationSpeed;
    public float distanceOffsetFromPlayer;
    [Range(0f, 1f)]
    public float lerpPercentage = 0f;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void LateUpdate()
    {  
        RotatePet();
        if(Vector3.Distance(player.position, transform.position) > distanceOffsetFromPlayer)
        {

            //transform.Translate(0, 0, movementSpeed * Time.deltaTime);
            Vector3 smoothLerp = Vector3.Lerp(this.transform.position, new Vector3(player.position.x, this.transform.position.y, player.position.z), lerpPercentage*Time.deltaTime);
            transform.position = smoothLerp;
           
        }
    }

    void RotatePet()
    {
        Vector3 faceOwner = new Vector3(player.position.x,
                               this.transform.position.y,
                               player.position.z);

        Vector3 lookDirection = faceOwner - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                  Quaternion.LookRotation(lookDirection),
                                  Time.deltaTime * rotationSpeed);

    }



}
