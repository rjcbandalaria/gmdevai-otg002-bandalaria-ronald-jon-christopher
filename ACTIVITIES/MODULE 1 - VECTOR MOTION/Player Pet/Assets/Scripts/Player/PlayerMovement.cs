﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float movementSpeed = 5;
    public Rigidbody rigidBody;

    Vector3 movement;
    Vector3 moveVelocity;
    Vector3 playerInput;

    // Update is called once per frame
    void Update()
    {
       
    }

    private void FixedUpdate()
    {
         PlayerInput();
        //rigidBody.MovePosition(rigidBody.position + movement.normalized * movementSpeed * Time.fixedDeltaTime);
        rigidBody.MovePosition(rigidBody.position + moveVelocity * Time.fixedDeltaTime);
     
    }
    
    void PlayerInput()
    {
        //Input 
        movement.x = Input.GetAxisRaw("Horizontal"); // returns -1 if going to the left; returns 1 if going to the right 
        movement.z = Input.GetAxisRaw("Vertical"); // returns -1  if going down ; returns 1 if going up
                                                   // returns 0 if player is still 

        playerInput = new Vector3(movement.x, 0, movement.z);
        moveVelocity = playerInput.normalized * movementSpeed;
        // add .normalized for diagonal movement 
    }
}
