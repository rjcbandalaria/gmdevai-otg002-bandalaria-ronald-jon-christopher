﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTransformMovement : MonoBehaviour
{
    public float movementSpeed;

    Vector3 playerInput;
    Vector3 moveVelocity;
    Vector3 playerMovement; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate()
    {
        
    }

    void PlayerMove()
    {

        playerInput.x = Input.GetAxisRaw("Horizontal");
        playerInput.z = Input.GetAxisRaw("Vertical");

        playerMovement = new Vector3(playerInput.x, 0, playerInput.z);
        moveVelocity = playerMovement.normalized * movementSpeed;


    }
}
