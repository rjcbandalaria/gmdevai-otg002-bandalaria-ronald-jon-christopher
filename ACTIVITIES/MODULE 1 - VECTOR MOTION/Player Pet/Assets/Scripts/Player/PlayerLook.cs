﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public float mouseSensitivity = 100;
    public Transform playerBody; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LookMouse();
    }

    private void LateUpdate()
    {
        
    }

    void LookMouse()
    {
        //Get Player Mouse Input
        Vector3 mousePosition = Input.mousePosition;

        //Convert Mouse Pixel Coordinates to World Point 
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //Location of the mouse
        Vector3 playerDirection = new Vector3(mousePosition.x, 0, mousePosition.z);

        //Vector3 lookDirection = playerDirection - playerBody.position;

        //Distance from the Mouse Position to the Player 
        Vector3 lookDirection = new Vector3(mousePosition.x - this.transform.position.x,
         transform.position.y , mousePosition.z - this.transform.position.z);
        //Look at the Direction of the Mouse 

        playerBody.transform.LookAt(lookDirection);

       // transform.LookAt(new Vector3(mousePosition.x - playerBody.position.x, playerBody.transform.position.y, mousePosition.z- playerBody.position.z));
     
    }
}
