﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TankState
{
    Patrol,
    Chase,
    Attack,
    Flee

}

public class TankAI : MonoBehaviour
{

    Animator animator;

    public GameObject player;
    public GameObject bullet;
    public GameObject turret;

    TankState tankState;

    public GameObject GetPlayer()
    {
        return player;
    }
    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            animator.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
            animator.SetFloat("playerHealth", player.GetComponent<PlayerHealth>().GetHealth());
            animator.SetBool("isPlayerDead", false);
        }
        
        if(player == null)
        {
            Debug.Log("Player is dead");
            animator.SetBool("isPlayerDead", true);
        }
  
    }

    void Fire()
    {
        GameObject projectile = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        projectile.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, 0.5f);
    }


}
