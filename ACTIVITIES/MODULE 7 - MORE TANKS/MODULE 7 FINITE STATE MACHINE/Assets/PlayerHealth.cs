﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    
    public int healthPoints;
    public int maxHealthPoints = 10;

    bool isDead;

    // Start is called before the first frame update
    void Start()
    {
       // isDead = false;
        healthPoints = maxHealthPoints;
        
    }

    public int GetHealth()
    {
        return healthPoints;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isPlayerDead();
    }

    public void takeDamage(int damage)
    {
        //if (isPlayerDead())
        //{
        //    return;
        //}
        //if(healthPoints <= 0)
        //{
        //    Destroy(gameObject);
        //}
        healthPoints = healthPoints - damage;

       
    }

    bool isPlayerDead()
    {
        if(healthPoints <= 0)
        {
            Destroy(gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }
  
}
