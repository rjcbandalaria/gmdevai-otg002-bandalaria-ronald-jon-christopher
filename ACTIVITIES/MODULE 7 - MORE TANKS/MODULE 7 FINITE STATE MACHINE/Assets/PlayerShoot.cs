﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    
    public GameObject bullet;
    public GameObject turret;

    public float bulletSpeed = 1000f;
    public float damage = 1;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    void Shoot()
    {

        GameObject projectile = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        projectile.GetComponent<Rigidbody>().AddForce(turret.transform.forward * bulletSpeed);

    }
}
