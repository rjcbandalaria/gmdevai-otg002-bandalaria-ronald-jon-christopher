﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    Animator animator;
    public int hp = 5;
    public int maxHp = 5;

    // Start is called before the first frame update
    void Start()
    {
        hp = maxHp;
        animator = this.GetComponent<Animator>();
    }

    float GetHealthPercent()
    {
        return (float)hp / (float)maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("enemyHealth", GetHealthPercent());
    }

    public void takeDamage(int damage)
    {

        hp = hp - damage;

        if (hp <= 0)
        {
            Destroy(gameObject);
        }

    }
}
