﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;

    float speed = 5.0f;

    float accuracy = 1.0f;

    float rotationSpeed = 3.0f;

    public GameObject waypointManager;

    GameObject[] waypoints;

    GameObject currentNode;

    int currentWaypointIndex = 0;

    Graph graph;

    // Start is called before the first frame update
    void Start()
    {
        waypoints = waypointManager.GetComponent<WaypointManager>().waypoints;
        graph = waypointManager.GetComponent<WaypointManager>().graph;
        currentNode = waypoints[0];
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        //Check for graph 

        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        // the node we are closest to at the moment 
        currentNode = graph.getPathPoint(currentWaypointIndex);

        // if we are close enough to the current waypoint, move to the next one 
        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position,
            transform.position) < accuracy)
        {
            currentWaypointIndex++;

        }

        if (currentWaypointIndex < graph.getPathLength())
        {

            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);

            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction),
                Time.deltaTime * rotationSpeed);

            this.transform.Translate(0, 0, speed * Time.deltaTime);

        }



    }

    public void GoToHelipad()
    {
        graph.AStar(currentNode, waypoints[1]);

        currentWaypointIndex = 0;

    }

    public void GoToRuins()
    {

        graph.AStar(currentNode, waypoints[15]);

        currentWaypointIndex = 0;

    }

    public void GoToFactory()
    {

        graph.AStar(currentNode, waypoints[8]);

        currentWaypointIndex = 0;

    }

    public void GoToTwinMountain()
    {

        graph.AStar(currentNode, waypoints[9]);
        currentWaypointIndex = 0;

    }

    public void GoToCommandCenter()
    {

        graph.AStar(currentNode, waypoints[14]);
        currentWaypointIndex = 0;

    }

    public void GoToBarracks()
    {

        graph.AStar(currentNode, waypoints[22]);
        currentWaypointIndex = 0; 


    }

    public void GoToRefinery()
    {

        graph.AStar(currentNode, waypoints[36]);
        currentWaypointIndex = 0;

    }

    public void GoToMiddle()
    {

        graph.AStar(currentNode, waypoints[21]);
        currentWaypointIndex = 0;

    }

    public void GoToTanker()
    {

        graph.AStar(currentNode, waypoints[31]);
        currentWaypointIndex = 0;

    }

    public void GoToRadar()
    {

        graph.AStar(currentNode, waypoints[13]);

    }

}
