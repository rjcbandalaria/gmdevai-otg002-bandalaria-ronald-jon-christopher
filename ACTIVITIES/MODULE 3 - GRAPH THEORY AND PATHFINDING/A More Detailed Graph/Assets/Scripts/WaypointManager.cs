﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct Link
{
    public enum direction { UNI, BI }; // either unidirectional or bidirectional (directed and undirected) 

    public GameObject node1;

    public GameObject node2;

    public direction dir;

}

public class WaypointManager : MonoBehaviour
{
    public GameObject[] waypoints; // array of waypoints 

    [SerializeField]
    public Link[] links; // how the nodes will be connected 


    public Graph graph = new Graph();

    // Start is called before the first frame update
    void Start()
    {
        if (waypoints.Length > 0)// Checks if there are waypoints in the waypoint references
        {

            foreach (GameObject wp in waypoints)
            {
                graph.AddNode(wp);// Adding all the waypoints to the nodes inside the graph object 
            }

            foreach (Link l in links)
            {

                graph.AddEdge(l.node1, l.node2);// Adding all the links as edges to the graphs
                if (l.dir == Link.direction.BI) // Checks if bidirectional
                {
                    graph.AddEdge(l.node2, l.node1); // adding them in reverse
                }

            }

        }


    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Drawing");
        graph.debugDraw(); // draws the graph
    }
}
